import Hapi from "@hapi/hapi";
import Inert from "@hapi/inert";
import Vision from "@hapi/vision";
import HapiSwagger from "hapi-swagger";

import pkg from "./package.json";

const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost',
  });

  const swaggerOptions = {
    info: {
      title: 'Test API Documentation',
      version: pkg.version,
    },
  };

  await server.register([
    { plugin: Inert },
    { plugin: Vision },
    {
      plugin: HapiSwagger,
      options: swaggerOptions,
    },
  ]);


  server.route({
    method: 'GET',
    path: '/',
    handler: () => {
      return 'Hello World! Server here!';
    },
    options: {
      tags: ['api']
    }
  });

  await server.start();
  console.log(`Server started on  ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
